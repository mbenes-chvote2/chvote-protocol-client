import { Blake2bService } from '@protocoder/blake2';
import { Conversion } from "./conversion.algo";
import { Blake2bContext } from "@protocoder/blake2/src/@protocoder/blake2/src/blake2b-context";
import { BigInteger } from "../model/big-integer";
import { BigIntPair } from "../model/big-int-pair";
import { GeneralAlgorithms } from './general-algorithms.algo';

export class Hash {

  private readonly HASHING_KEY: Uint8Array = null;

  constructor(private blake2bService: Blake2bService,
              private upper_l: number) {
  }

  /**
   * Algorithm 4.9: RecHash_L, varargs version
   * <p>
   * Computes the hash value h(v_1, ..., v_k) in B^L of multiple inputs v_1, ..., v_k in a recursive manner.
   * </p>
   *
   * @param objects an array of objects to hash
   * @return The recursive hash as defined in section 4.3
   */
  public recHash_L(objects: any[]): Uint8Array {
    let hashContext: Blake2bContext = this.blake2bService.init(32);
    if (objects.length === 0) {
      return this.blake2bService.final(hashContext);

    } else if (objects.length === 1) {
      return this.recHash_L_single(objects[0]);

    } else {
      for (let object of objects) {
        this.blake2bService.update(hashContext, this.recHash_L_single(object));
      }
      let hash = this.blake2bService.final(hashContext);
      return GeneralAlgorithms.truncate(hash, this.upper_l);
    }
  }

  public recHash_L_single(object: any): Uint8Array {
    if (Array.isArray(object)) {
      return this.recHash_L(object);
    } else {
      return this.hash_L(object);
    }
  }

  /**
   * Use the underlying Blake2b algorithm to obtain a hash, truncated to length L
   *
   * @param toHash the object to be hashed
   * @return the hash of the provided object, truncated to L bytes
   */
  public hash_L(toHash: Uint8Array | string | BigInteger | BigIntPair): Uint8Array {
    if (toHash instanceof BigIntPair) {
      return this.recHash_L([toHash.left, toHash.right]);
    }

    let byteArray: Uint8Array;
    if (typeof toHash === 'string' || toHash instanceof BigInteger) {
      byteArray = Conversion.toByteArray(toHash);
    } else {
      byteArray = <Uint8Array> toHash;
    }

    let hash: Uint8Array = this.blake2bService.hash(byteArray, this.HASHING_KEY, 32);
    return GeneralAlgorithms.truncate(hash, this.upper_l);
  }
}
