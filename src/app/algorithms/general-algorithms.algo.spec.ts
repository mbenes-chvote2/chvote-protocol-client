import { EncryptionGroup } from '../model/encryption-group';
import { Hash } from './hash.algo';
import { IdentificationGroup } from '../model/identification-group';
import { PrimesCache } from '../model/primes-cache';
import { GeneralAlgorithms } from './general-algorithms.algo';
import { BigInteger } from "../model/big-integer";

describe('GeneralAlgorithms', () => {

  const THREE: BigInteger = new BigInteger(3);
  const FOUR: BigInteger = new BigInteger(4);
  const FIVE: BigInteger = new BigInteger(5);
  const NINE: BigInteger = new BigInteger(9);
  const ELEVEN: BigInteger = new BigInteger(11);
  const EG: EncryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FIVE);
  const IG: IdentificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE);

  let generalAlgorithms: GeneralAlgorithms;
  let hashMock: Hash;

  beforeAll(() => {
    hashMock = new Hash(null, 0);
    generalAlgorithms = new GeneralAlgorithms(
      hashMock,
      EG,
      IG,
      new PrimesCache([THREE, FIVE], EG)
    );
  });

  // isMember(LargeInteger)
  [
    [1, true],
    [11, false]
  ].forEach(([_x, expected]) => {
    it(`isMember(${_x}) should return [${expected}]`, () => {
      let x: BigInteger = new BigInteger(<number>_x);
      expect(generalAlgorithms.isMember(x)).toBe(<boolean>expected);
    });
  });

  it('getPrimes', () => {
    let primes: BigInteger[] = generalAlgorithms.getPrimes(2);
    expect(primes.length).toBe(2);
    expect(primes).toEqual([THREE, FIVE]);
  });

  it('getGenerators', () => {
    spyOn(hashMock, 'recHash_L').and.returnValues(
      [0x09], // 9 * 9 = 81 =_11 4 --> OK
      [0x01], // 1 * 1 = 1 =_11 1 --> KO, is 1
      [0x03] // 3 * 3 = 9 =_11 9 --> OK
    );
    let generators: BigInteger[] = generalAlgorithms.getGenerators(2);
    expect(generators.length).toBe(2);
    expect(generators[0].equals(FOUR)).toBeTruthy(`expected ${generators[0].toHexString()} to equal 4`);
    expect(generators[1].equals(NINE)).toBeTruthy(`expected ${generators[1].toHexString()} to equal 9`);
  });

  it('getNIZKPChallenge', () => {
    spyOn(hashMock, 'recHash_L').and.returnValue([0x0F]);
    let nizkpChallenge = generalAlgorithms.getNIZKPChallenge([], [], 2);
    expect(nizkpChallenge.equals(THREE)).toBeTruthy(`expected ${nizkpChallenge.toHexString()} to equal 3`);
  });

  it('getNIZKPChallenges', () => {
    spyOn(hashMock, 'recHash_L').and.returnValue([0x00]);
    spyOn(hashMock, 'hash_L').and.callFake(function (value: Uint8Array | BigInteger) {
      if (value instanceof BigInteger) {
        if (value.equals(BigInteger.ONE)) {
          return [0x01];
        } else if (value.equals(BigInteger.TWO)) {
          return [0x02];
        } else if (value.equals(THREE)) {
          return [0x03];
        }
      } else {
        if (value[1] === 0x01) {
          return new Uint8Array([0x0A]);
        } else if (value[1] === 0x02) {
          return new Uint8Array([0x03]);
        } else if (value[1] === 0x03) {
          return new Uint8Array([0x1F]);
        }
      }
    });

    let challenges: BigInteger[] = generalAlgorithms.getNIZKPChallenges(3, [], 2);
    expect(challenges.length).toBe(3);
    expect(challenges[0].equals(BigInteger.TWO)).toBeTruthy(`expected ${challenges[0].toHexString()} to equal 2`);
    expect(challenges[1].equals(THREE)).toBeTruthy(`expected ${challenges[1].toHexString()} to equal 3`);
    expect(challenges[2].equals(THREE)).toBeTruthy(`expected ${challenges[2].toHexString()} to equal 3`);
  });

  // markByteArray
  [
    [[0x00], 0, 3, [0x00]],
    [[0x00], 1, 3, [0x01]],
    [[0x00], 2, 3, [0x10]],
    [[0x00], 3, 3, [0x11]],
    [[0xFF, 0xFF, 0xFF, 0xFF], 0, 3, [0xFE, 0xFF, 0xFE, 0xFF]],
    [[0xC1, 0xD2], 0, 3, [0xC0, 0xD2]],
    [[0xC1, 0xD2], 1, 3, [0xC1, 0xD2]],
    [[0xC1, 0xD2], 0, 15, [0xC0, 0xC2]],
    [[0xCC, 0xDD], 0, 15, [0xCC, 0xCC]]
  ].forEach(([upper_b, m, m_max, expected]) => {
    it(`markByteArray([${upper_b}], ${m}, ${m_max}) should return [${expected}]`, () => {
      let _upper_b: Uint8Array = new Uint8Array(upper_b as number[]);
      let _expected: Uint8Array = new Uint8Array(expected as number[]);
      expect(GeneralAlgorithms.markByteArray(_upper_b, m as number, m_max as number).toString()).toEqual(_expected.toString());
    });
  });

  it('markByteArray([0], 2, 3) should return [16]', () => {
    let _upper_b: Uint8Array = new Uint8Array([0] as number[]);
    let _expected: Uint8Array = new Uint8Array([16] as number[]);
    expect(GeneralAlgorithms.markByteArray(_upper_b, 2, 3).toString()).toEqual(_expected.toString());
  });

});
