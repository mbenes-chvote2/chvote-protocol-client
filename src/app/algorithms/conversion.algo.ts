import { Preconditions } from "../tools/preconditions";
import { BigInteger } from "../model/big-integer";

/**
 * This class handles the conversions between strings, byte arrays and integers
 */
export class Conversion {

  constructor() {
  }

  /**
   * Algorithm 4.3 and 4.4: ToByteArray
   *
   * @param x the integer or string to be converted
   * @param n the target length (in bytes)
   * @return the converted value, left-padded with <tt>0</tt>s if length is smaller than target, or truncated left if
   * it's larger
   */
  static toByteArray(x: BigInteger | string, n?: number): Uint8Array {
    if (typeof x === 'string') {
      return new Uint8Array(Buffer.from(x, 'utf-8'));

    } else {
      if (n === undefined) {
        n = x.cmp(BigInteger.ZERO) === 0 ? 0 : Math.ceil(x.bitLength() / 8.0);
      }

      if (n === 0 && x.cmp(BigInteger.ZERO) === 0) {
        return new Uint8Array(0);
      }

      return x.toByteArray(n);
    }
  }

  /**
   * Algorithm 4.5: ToInteger
   *
   * @param byteArray the byte array or string to be converted
   * @param upper_a the alphabet to be used
   * @return the corresponding integer (unsigned, non-injective conversion)
   */
  static toInteger(byteArray: Uint8Array | string, upper_a?: Uint8Array): BigInteger {
    if (typeof byteArray === 'string') {
      let upper_n: BigInteger = new BigInteger(upper_a.length);
      let x: BigInteger = BigInteger.ZERO;
      for (let i: number = 0; i < byteArray.length; i++) {
        let rank_upper_a: number = this.indexOf(upper_a, byteArray.charCodeAt(i));
        Preconditions.checkArgument(rank_upper_a >= 0, `character ${byteArray.charAt(i)} not found in alphabet ${upper_a}`);
        x = x.mul(upper_n).add(new BigInteger(rank_upper_a));
      }
      return x;

    } else {
      let bytes: number[] = [0];
      for (let i: number = 0; i < byteArray.length; i++) {
        bytes.push(byteArray[i]);
      }
      return new BigInteger(bytes);
    }
  }

  private static indexOf(array: Uint8Array, index: number) {
    if (array.indexOf) {
      return array.indexOf(index);
    } else {
      // some browsers like IE8 and previous does not support indexOf function
      let i: number;
      for (i = 0; i < array.length; i++) {
        if (array[i] == index) break;
      }
      return array[i] == index ? i : -1;
    }
  }

  /**
   * Algorithm 4.6: ToString
   *
   * @param upper_b the byte array to convert
   * @param upper_a the alphabet to be used
   * @return a string of length k, using alphabet A, and representing x
   */
  public static toString(upper_b: Uint8Array, upper_a: Uint8Array): string {
    let _upper_b: BigInteger = Conversion.toInteger(upper_b);
    let k: number = Math.ceil(8.0 * upper_b.length / (Math.log(upper_a.length) / Math.log(2)));

    if (k === 0) {
      return '';
    }

    Preconditions.checkArgument(_upper_b.cmp(BigInteger.ZERO) >= 0, 'x should be a non-negative integer');

    let alphabetSize: number = upper_a.length;
    let upper_n: BigInteger = new BigInteger(alphabetSize);
    let upper_n_pow: BigInteger = new BigInteger(Math.pow(alphabetSize, k));
    Preconditions.checkArgument(upper_n_pow.cmp(_upper_b) >= 0, 'x is too large to be encoded with k characters of alphabet upper_a');

    let s: string = '';

    let current: BigInteger = _upper_b;
    for (let i: number = 1; i <= k; i++) {
      let divideAndRemainder: BigInteger[] = current.divQR(upper_n);
      current = divideAndRemainder[0];
      // always insert before the previous character
      s = String.fromCharCode(upper_a[divideAndRemainder[1].toByteArray()[0]]).concat(s);
    }

    return s;
  }
}
