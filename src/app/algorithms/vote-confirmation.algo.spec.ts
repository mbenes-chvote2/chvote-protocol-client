import { VoteConfirmation } from './vote-confirmation.algo';
import { SecurityParameters } from "../model/security-parameters";
import { EncryptionGroup } from "../model/encryption-group";
import { RandomGenerator } from "./random-generator.algo";
import { IdentificationGroup } from "../model/identification-group";
import { PublicParameters } from "../model/public-parameters";
import { GeneralAlgorithms } from "./general-algorithms.algo";
import { Hash } from "./hash.algo";
import { PrimesCache } from "../model/primes-cache";
import { Point } from "../model/point";
import { NonInteractiveZkp } from "../model/non-interactive-zkp";
import { FinalizationCodePart } from "../model/finalization-code-part";
import { BigIntPair } from "../model/big-int-pair";
import { BigInteger } from "../model/big-integer";
import * as _ from "underscore";

describe('VoteConfirmation', () => {

  const ZERO: BigInteger = BigInteger.ZERO;
  const ONE: BigInteger = BigInteger.ONE;
  const TWO: BigInteger = BigInteger.TWO;
  const THREE: BigInteger = new BigInteger(3);
  const FOUR: BigInteger = new BigInteger(4);
  const FIVE: BigInteger = new BigInteger(5);
  const SIX: BigInteger = new BigInteger(6);
  const SEVEN: BigInteger = new BigInteger(7);
  const NINE: BigInteger = new BigInteger(9);
  const ELEVEN: BigInteger = new BigInteger(11);

  const DEFAULT_ALPHABET: Uint8Array = _.map('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_'.split(''), function (char: string) {
    return char.charCodeAt(0);
  });

  const ENCRYPTION_GROUP: EncryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR);
  const IDENTIFICATION_GROUP: IdentificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE);
  const SECURITY_PARAMETERS: SecurityParameters = new SecurityParameters(1, 1, 2, 0.99);

  const PUBLIC_PARAMETERS: PublicParameters = new PublicParameters(
    SECURITY_PARAMETERS,
    ENCRYPTION_GROUP,
    IDENTIFICATION_GROUP,
    SEVEN,
    FIVE,
    DEFAULT_ALPHABET,
    FIVE,
    DEFAULT_ALPHABET,
    DEFAULT_ALPHABET,
    2,
    DEFAULT_ALPHABET,
    2,
    4,
    5
  );

  let hashMock: Hash;
  let generalAlgorithmsMock: GeneralAlgorithms;
  let voteConfirmation: VoteConfirmation;

  beforeAll(() => {
    hashMock = new Hash(null, 0);
    generalAlgorithmsMock = new GeneralAlgorithms(null, ENCRYPTION_GROUP, IDENTIFICATION_GROUP, new PrimesCache([], ENCRYPTION_GROUP));
    voteConfirmation = new VoteConfirmation(PUBLIC_PARAMETERS, generalAlgorithmsMock);
  });

  it('genConfirmation should generate the expected confirmation', () => {
    // given a given set of parameters
    let confirmationCode: string = "cA"; // ToInteger(Y) = 154
    let upper_bold_p: Point[][] = [
      [new Point(FOUR, TWO)],
      [new Point(THREE, ONE)],
      [new Point(FIVE, ZERO)],
      [new Point(SIX, THREE)]
    ];

    // and known collaborators responses
    spyOn(RandomGenerator, 'randomInZq').and.returnValue(THREE); // called by GenConfirmationProof - omega
    spyOn(generalAlgorithmsMock, 'getNIZKPChallenge').and.returnValue(ONE); // c

    // and the expected preconditions checks
    spyOn(generalAlgorithmsMock, 'isMember_G_q_hat').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isInZ_q_hat').and.callThrough();

    // when
    let confirmation = voteConfirmation.genConfirmation(confirmationCode, upper_bold_p);

    // then
    // y' = getValue(upper_bold_p) = 1
    // y_hat = g_hat ^ (y + y') mod p_hat = 3 ^ 5 mod 11 = 1
    // t = g_hat ^ omega mod p_hat = 3 ^ 3 mod 11 = 5
    // s = omega + c * (y + y') mod q_hat = 3 + 1 * (4 + 1) mod 5 = 3
    // y = 154 mod 5 = 4
    expect(confirmation.y_hat.equals(ONE)).toBeTruthy(`expected ${confirmation.y_hat.toHexString()} to equal 1`);

    expect(confirmation.pi.t.length).toBe(1);
    expect(confirmation.pi.t[0].equals(FIVE)).toBeTruthy(`expected ${confirmation.pi.t[0].toHexString()} to equal 5`);

    expect(confirmation.pi.s.length).toBe(1);
    expect(confirmation.pi.s[0].equals(THREE)).toBeTruthy(`expected ${confirmation.pi.s[0].toHexString()} to equal 3`);
  });

  [
    [[new Point(SIX, ONE)], ONE],
    [[new Point(THREE, TWO), new Point(FIVE, ONE)], ZERO]
  ].forEach(([points, y]) => {
    it(`getValue(${points}) should return [${y}]`, () => {
      let value: BigInteger = voteConfirmation.getValue(<Point[]>points);
      expect(value.equals(<BigInteger>y)).toBeTruthy(`expected ${value.toHexString()} to equal ${(<BigInteger>y).toHexString()}`);
    });
  });

  it('genConfirmationProof should generate a valid proof of knowledge for y', () => {
    // given a known random omega
    spyOn(RandomGenerator, 'randomInZq').and.returnValue(FOUR); // omega

    // and a known challenge value
    // t = g_hat ^ omega mod p_hat = 3 ^ 4 mod 11 = 4
    spyOn(generalAlgorithmsMock, 'getNIZKPChallenge').and.returnValue(ONE);

    // and the expected preconditions checks
    spyOn(generalAlgorithmsMock, 'isMember_G_q_hat').and.returnValue(true);
    spyOn(generalAlgorithmsMock, 'isInZ_q_hat').and.callThrough();

    // expect the generated proof to have the expected value
    // s = omega + c * (y + y_prime) mod q_hat = 4 + 1 * (2 + 4) mod 5 = 0
    let proof: NonInteractiveZkp = voteConfirmation.genConfirmationProof(TWO, NINE, FOUR);
    expect(proof.t.length).toBe(1);
    expect(proof.t[0].equals(FOUR)).toBeTruthy(`expected ${proof.t[0].toHexString()} to equal 4`);
    expect(proof.s.length).toBe(1);
    expect(proof.s[0].equals(ZERO)).toBeTruthy(`expected ${proof.s[0].toHexString()} to equal 0`);
  });

  it('getFinalizationCode should correctly combine the given finalization code parts', () => {
    expect(voteConfirmation.getFinalizationCode([
      new FinalizationCodePart(new Uint8Array([0xDE, 0xAD]), new BigIntPair(ONE, ZERO)),
      new FinalizationCodePart(new Uint8Array([0xBE, 0xEF]), new BigIntPair(ONE, ZERO)),
      new FinalizationCodePart(new Uint8Array([0x01, 0x10]), new BigIntPair(ONE, ZERO)),
      new FinalizationCodePart(new Uint8Array([0xFA, 0xCE]), new BigIntPair(ONE, ZERO))
    ])).toEqual("jUC"); // [0x9B, 0x9C] -> 39836
  });
});
