import { PublicParameters } from "../model/public-parameters";
import { GeneralAlgorithms } from "./general-algorithms.algo";
import { Point } from "../model/point";
import { Confirmation } from "../model/confirmation";
import { FinalizationCodePart } from "../model/finalization-code-part";
import { NonInteractiveZkp } from "../model/non-interactive-zkp";
import { Preconditions } from "../tools/preconditions";
import { Conversion } from "./conversion.algo";
import { RandomGenerator } from "./random-generator.algo";
import { BigInteger } from "../model/big-integer";
import * as _ from "underscore";

export class VoteConfirmation {

  private static ALL_POINTS_COORDINATES_MUST_BE_IN_Z_P_PRIME: string = `All points' coordinates must be in Z_p_prime`;

  constructor(private publicParameters: PublicParameters,
              private generalAlgorithms: GeneralAlgorithms) {
  }

  /**
   * Algorithm 7.30: GenConfirmation
   *
   * @param upper_y the confirmation code
   * @param bold_upper_p the s< per k point matrix, where k = sum(bold_k) and s is the number of authorities
   * @return the public confirmation y_hat and the proof of knowledge of the secret confirmation y
   */
  public genConfirmation(upper_y: string, bold_upper_p: Point[][]): Confirmation {
    Preconditions.checkNotNull(upper_y);
    Preconditions.checkNotNull(bold_upper_p);
    Preconditions.checkArgument(bold_upper_p.length == this.publicParameters.s, 'There should be one list of points per authority');

    let p_prime: BigInteger = this.publicParameters.p_prime;

    Preconditions.checkArgument(_.every(bold_upper_p, function (points: Point[]) {
      return _.every(points, function (point: Point) {
        return BigInteger.ZERO.cmp(point.x) <= 0
          && point.x.cmp(p_prime) < 0
          && BigInteger.ZERO.cmp(point.y) <= 0
          && point.y.cmp(p_prime) < 0;
      })
    }), VoteConfirmation.ALL_POINTS_COORDINATES_MUST_BE_IN_Z_P_PRIME);


    let p_hat: BigInteger = this.publicParameters.identificationGroup.p_hat;
    let q_hat: BigInteger = this.publicParameters.identificationGroup.q_hat;
    let g_hat: BigInteger = this.publicParameters.identificationGroup.g_hat;

    let y: BigInteger = Conversion.toInteger(upper_y, this.publicParameters.upper_a_y).mod(q_hat);

    let y_prime: BigInteger = BigInteger.ZERO;
    for (let i: number = 0; i < bold_upper_p.length; i++) {
      y_prime = y_prime.add(this.getValue(bold_upper_p[i]));
    }
    y_prime = y_prime.mod(q_hat);

    let y_hat: BigInteger = g_hat.modPow(y.add(y_prime).mod(q_hat), p_hat);
    let pi: NonInteractiveZkp = this.genConfirmationProof(y, y_hat, y_prime);

    return new Confirmation(y_hat, pi);
  }

  /**
   * Algorithm 7.31: GetValue
   *
   * @param bold_p a list of k points defining the polynomial A(X) of degree k - 1
   * @return the interpolated value y = A(0)
   */
  public getValue(bold_p: Point[]): BigInteger {
    Preconditions.checkNotNull(bold_p);
    let p_prime: BigInteger = this.publicParameters.p_prime;
    Preconditions.checkArgument(_.every(bold_p, function (point: Point) {
      return BigInteger.ZERO.cmp(point.x) <= 0
        && point.x.cmp(p_prime) < 0
        && BigInteger.ZERO.cmp(point.y) <= 0
        && point.y.cmp(p_prime) < 0;
    }), VoteConfirmation.ALL_POINTS_COORDINATES_MUST_BE_IN_Z_P_PRIME);

    let y: BigInteger = BigInteger.ZERO;
    for (let i: number = 0; i < bold_p.length; i++) {
      let n: BigInteger = BigInteger.ONE;
      let d: BigInteger = BigInteger.ONE;
      for (let j: number = 0; j < bold_p.length; j++) {
        if (i != j) {
          let x_i: BigInteger = bold_p[i].x;
          let x_j: BigInteger = bold_p[j].x;

          n = n.mul(x_j).mod(p_prime);
          d = d.mul(x_j.sub(x_i)).mod(p_prime);
        }
      }
      y = y.add(bold_p[i].y.mul(n.mul(d.modInv(p_prime)))).mod(p_prime);
    }

    return y;
  }

  /**
   * Algorithm 7.32: GenConfirmationProof
   *
   * @param y       the secret confirmation credential
   * @param y_hat   the public confirmation credential
   * @param y_prime the secret vote validity credential
   * @return a proof of knowledge of the secret confirmation credential
   */
  public genConfirmationProof(y: BigInteger, y_hat: BigInteger, y_prime: BigInteger): NonInteractiveZkp {
    let p_hat: BigInteger = this.publicParameters.identificationGroup.p_hat;
    let q_hat: BigInteger = this.publicParameters.identificationGroup.q_hat;
    let g_hat: BigInteger = this.publicParameters.identificationGroup.g_hat;
    let tau: number = this.publicParameters.securityParameters.tau;

    //noinspection SuspiciousNameCombination
    Preconditions.checkArgument(this.generalAlgorithms.isInZ_q_hat(y), 'y must be in Z_q_hat');
    //noinspection SuspiciousNameCombination
    Preconditions.checkArgument(this.generalAlgorithms.isMember_G_q_hat(y_hat), 'y_hat must be in G_q_hat');

    let omega: BigInteger = RandomGenerator.randomInZq(q_hat);

    let t: BigInteger = g_hat.modPow(omega, p_hat);
    let bold_v: BigInteger[] = [y_hat];
    let bold_t: BigInteger[] = [t];
    let c: BigInteger = this.generalAlgorithms.getNIZKPChallenge(bold_v, bold_t, tau);
    let s: BigInteger = omega.add(c.mul(y.add(y_prime))).mod(q_hat);
    return new NonInteractiveZkp([t], [s]);
  }

  /**
   * Algorithm 7.37: GetFinalizationCode
   *
   * @param bold_delta the finalization code parts received from the authorities
   * @return the combined finalization code
   */
  public getFinalizationCode(bold_delta: FinalizationCodePart[]): string {
    Preconditions.checkArgument(bold_delta.length == this.publicParameters.s);

    let code: Uint8Array = bold_delta[0].upper_f;
    for (let i: number = 1; i < bold_delta.length; i++) {
      code = GeneralAlgorithms.xor(code, bold_delta[i].upper_f);
    }

    return Conversion.toString(code, this.publicParameters.upper_a_f);
  }
}
