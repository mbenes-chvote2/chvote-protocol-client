import { Preconditions } from "../tools/preconditions";
import { BigInteger } from "./big-integer";
import { EncryptionGroupJSON } from "./encryption-group-json";

export class EncryptionGroup {

  constructor(public readonly p: BigInteger,
              public readonly q: BigInteger,
              public readonly g: BigInteger,
              public readonly h: BigInteger) {
    Preconditions.checkArgument(q.bitLength() == p.bitLength() - 1);
    Preconditions.checkArgument(g.cmp(BigInteger.ONE) > 0);
    Preconditions.checkArgument(h.cmp(BigInteger.ONE) > 0);
  }

  public equals(other: EncryptionGroup): boolean {
    if (this === other) {
      return true;
    } else if (other !== null) {
      return this.p.equals(other.p) &&
        this.q.equals(other.q) &&
        this.g.equals(other.g) &&
        this.h.equals(other.h);
    } else {
      return false;
    }
  }

  public toString(): string {
    return `EncryptionGroup{p=${this.p.toHexString()}, q=${this.q.toHexString()}, g=${this.g.toHexString()}, h=${this.h.toHexString()}`;
  }

  static fromJSON(json: EncryptionGroupJSON | string): EncryptionGroup {
    if (typeof json === 'string') {
      return JSON.parse(json, EncryptionGroup.reviver);

    } else {
      return new EncryptionGroup(
        new BigInteger((<EncryptionGroupJSON>json).p),
        new BigInteger((<EncryptionGroupJSON>json).q),
        new BigInteger((<EncryptionGroupJSON>json).g),
        new BigInteger((<EncryptionGroupJSON>json).h)
      );

    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? EncryptionGroup.fromJSON(value) : value;
  }
}
