import { BigIntPair } from "./big-int-pair";
import { BigInteger } from "./big-integer";

export class ObliviousTransfertQuery {

  constructor(public readonly bold_a: BigIntPair[],
              public readonly bold_r: BigInteger[]) {
  }
}
