import { BallotAndQuery } from "./ballot-and-query";
import { BigInteger } from "./big-integer";

export class BallotQueryAndRand {

  constructor(public readonly alpha: BallotAndQuery,
              public readonly bold_r: BigInteger[]) {
  }
}
