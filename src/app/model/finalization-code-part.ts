import { BigIntPair } from "./big-int-pair";
import { FinalizationCodePartJSON } from "./finalization-code-part-json";
import { CiphertextMatrix } from "./ciphertext-matrix";

export class FinalizationCodePart {

  constructor(public readonly upper_f: Uint8Array,
              public readonly z: BigIntPair) {
  }

  static fromJSON(json: FinalizationCodePartJSON | string): FinalizationCodePart {
    if (typeof json === 'string') {
      return JSON.parse(json, FinalizationCodePart.reviver);

    } else {
      return new FinalizationCodePart(
        CiphertextMatrix.base64ToUint8Array(json.upper_f),
        json.z ? BigIntPair.fromJSON(json.z) : null
      );
    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? FinalizationCodePart.fromJSON(value) : value;
  }
}
