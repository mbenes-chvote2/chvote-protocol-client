import { BigInteger } from './big-integer';
import { BigIntPairJSON } from './big-int-pair-json';
import { IdentificationGroup } from "./identification-group";

export class BigIntPair {

  constructor(public readonly left: BigInteger,
              public readonly right: BigInteger) {
  }

  toJSON(): BigIntPairJSON {
    return new BigIntPairJSON(this.left.toBase64String(), this.right.toBase64String());
  }

  static fromJSON(json: BigIntPairJSON | string): BigIntPair {
    if (typeof json === 'string') {
      return JSON.parse(json, IdentificationGroup.reviver);

    } else {
      return new BigIntPair(
        new BigInteger((<BigIntPairJSON>json).left),
        new BigInteger((<BigIntPairJSON>json).right)
      );

    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? BigIntPair.fromJSON(value) : value;
  }
}
