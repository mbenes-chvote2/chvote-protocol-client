import { CiphertextMatrix } from "./ciphertext-matrix";
import { BigInteger } from "./big-integer";
import { ObliviousTransferResponseJSON } from "./oblivious-transfer-response-json";

export class ObliviousTransferResponse {

  constructor(public readonly bold_b: BigInteger[],
              public readonly bold_upper_c: CiphertextMatrix,
              public readonly d: BigInteger) {
  }

  static fromJSON(json: ObliviousTransferResponseJSON | string): ObliviousTransferResponse {
    if (typeof json === 'string') {
      return JSON.parse(json, ObliviousTransferResponse.reviver);

    } else {
      let obliviousTransferResponse = Object.create(ObliviousTransferResponse.prototype);

      return Object.assign(obliviousTransferResponse, json, {
        bold_b: json.bold_b.map(b => new BigInteger(b)),
        bold_upper_c: CiphertextMatrix.fromJSON(json.bold_upper_c),
        d: new BigInteger(json.d)
      });
    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? ObliviousTransferResponse.fromJSON(value) : value;
  }
}
