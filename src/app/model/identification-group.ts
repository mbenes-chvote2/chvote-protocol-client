import { Preconditions } from "../tools/preconditions";
import { BigInteger } from "./big-integer";
import { IdentificationGroupJSON } from "./identification-group-json";

export class IdentificationGroup {

  constructor(public readonly p_hat: BigInteger,
              public readonly q_hat: BigInteger,
              public readonly g_hat: BigInteger) {
    Preconditions.checkArgument(q_hat.bitLength() <= p_hat.bitLength(), 'q should be lesser or equals to p');
    Preconditions.checkArgument(g_hat.cmp(BigInteger.ONE) != 0, 'g should be different from 1');
    Preconditions.checkArgument(p_hat.sub(BigInteger.ONE).mod(q_hat).cmp(BigInteger.ZERO) == 0, '(p - 1) mod q should be equals to 0');
  }

  static fromJSON(json: IdentificationGroupJSON | string): IdentificationGroup {
    if (typeof json === 'string') {
      return JSON.parse(json, IdentificationGroup.reviver);

    } else {
      return new IdentificationGroup(
        new BigInteger((<IdentificationGroupJSON>json).p_hat),
        new BigInteger((<IdentificationGroupJSON>json).q_hat),
        new BigInteger((<IdentificationGroupJSON>json).g_hat)
      );

    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? IdentificationGroup.fromJSON(value) : value;
  }
}
