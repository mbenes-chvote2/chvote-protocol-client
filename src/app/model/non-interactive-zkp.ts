import { BigInteger } from './big-integer';
import { NonInteractiveZkpJson } from './non-interactive-zkp-json';

export class NonInteractiveZkp {

  constructor(public readonly t: BigInteger[],
              public readonly s: BigInteger[]) {
  }

  toJSON(): NonInteractiveZkpJson {
    return new NonInteractiveZkpJson(
      this.t.map(value => value.toBase64String()),
      this.s.map(value => value.toBase64String())
    );
  }
}
